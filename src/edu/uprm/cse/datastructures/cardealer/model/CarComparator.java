package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarComparator<Object> implements Comparator<Object> {

	@Override
	public int compare(Object car1, Object car2) {
		Car a = (Car) car1;
		Car b = (Car) car2;
		String firstCar = a.getCarBrand() + a.getCarModel() + a.getCarModelOption();
		String secondCar = b.getCarBrand() + b.getCarModel() + b.getCarModelOption();

		return firstCar.compareTo(secondCar);

	}

}