package edu.uprm.cse.datastructures.cardealer.model;


import java.util.Iterator;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;


@Path("/cars")
public class CarManager {

	private static final CircularSortedDoublyLinkedList<Car> cList = new CircularSortedDoublyLinkedList<Car>();

	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {
		Car[] cArr = new Car[cList.size()];
		for (int i = 0; i < cList.size(); i++) {
			cArr[i] = cList.get(i);
		}

		return cArr;
	}

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long n) {

		for (int i = 0; i < cList.size(); i++) {
			if (cList.get(i).getCarId() == n)
				return cList.get(i);
		}

		throw new NotFoundException();
	}

	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car nCar) {

		cList.add(nCar);
		return Response.status(Response.Status.OK).build();
	}

	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(Car uCar, @PathParam("id") long a) {

		cList.remove(this.getCar(a));
		cList.add(uCar);

		return Response.status(Response.Status.OK).build();

	}

	@DELETE
	@Path("/{id}/delete")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteCar(@PathParam("id") long b) {
		cList.remove(this.getCar(b));

		return Response.status(Response.Status.NOT_FOUND).build();
	}
}
