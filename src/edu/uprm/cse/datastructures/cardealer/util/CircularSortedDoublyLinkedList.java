package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Iterator;
import java.util.NoSuchElementException;

import edu.uprm.cse.datastructures.cardealer.model.CarComparator;

public class CircularSortedDoublyLinkedList<Car> implements SortedList<Car> {

	private int size;
	private DNode<Car> header;

	@SuppressWarnings("hiding")
	private class DNode<Car> {
		Car NCar;
		DNode<Car> prev, next;

		public DNode(Car c) {
			NCar = c;
			prev = next = null;
		}

		public DNode(Car c, DNode<Car> p, DNode<Car> n) {
			NCar = c;
			prev = p;
			next = n;
		}

		public void setNext(DNode<Car> a) {
			this.next = a;
		}

		public void setPrev(DNode<Car> pr) {
			this.prev = pr;
		}

		public DNode<Car> getNext() {
			return this.next;
		}

		public DNode<Car> getPrev() {
			return this.prev;
		}

		public Car getElement() {
			return this.NCar;
		}

		public void clear() {
			this.next = this.prev = null;
			this.NCar = null;
		}
	}

	private class CSLLIterator<Car> implements Iterator<Car> {
		private DNode<Car> nextDNode;

		public CSLLIterator() {
			this.nextDNode = (DNode<Car>) header.getNext();
		}

		@Override
		public boolean hasNext() {
			return nextDNode != header;
		}

		@Override
		public Car next() {
			if (this.hasNext()) {
				Car result = this.nextDNode.getElement();
				this.nextDNode = this.nextDNode.getNext();
				return result;
			} else {
				throw new NoSuchElementException();
			}
		}

	}

	public CircularSortedDoublyLinkedList() {
		this.size = 0;
		this.header = new DNode<Car>(null);
	}

	@Override
	public Iterator<Car> iterator() {
		return new CSLLIterator<Car>();
	}

	@Override
	public boolean add(Car obj) {
		// empty list
		if (this.isEmpty()) {// if the list is empty
			DNode<Car> newCar = new DNode<Car>(obj, header, header);
			this.header.setNext(newCar);
			this.header.setPrev(newCar);
			this.size++;
			return true;
		} else {
			CarComparator<Car> cComp = new CarComparator<Car>();
			DNode<Car> curr = header.getNext();
			for (int i = 0; i < this.size; i++) { // looking for larger element
				if (cComp.compare(obj, curr.getElement()) >= 0) {
					DNode<Car> nCar = new DNode<Car>(obj, curr.getPrev(), curr);
					curr.getPrev().setNext(nCar);
					curr.setPrev(nCar);
					this.size++;
					return true;
				} else
					curr = curr.getNext();
			}

			// haven't found larger element then obj is larger and goes last
			DNode<Car> newNode = new DNode<Car>(obj, header.getPrev(), header);
			header.getPrev().setNext(newNode);
			header.setPrev(newNode);
			this.size++;
			return true;

		}
	}

	@Override
	public int size() {
		return this.size;
	}

	@Override
	public boolean remove(Car obj) {
		if (!this.contains(obj))
			return false;
		return this.remove(this.firstIndex(obj));
	}

	@Override
	public boolean remove(int index) {
		if (index < 0 || index >= this.size)
			throw new IndexOutOfBoundsException();

		int count = 0;
		DNode<Car> carrNode = header.getNext();
		DNode<Car> prv, nxt;
		while (count != index) {
			carrNode = carrNode.getNext();
			count++;
		}
		prv = carrNode.getPrev();
		nxt = carrNode.getNext();
		prv.setNext(nxt);
		nxt.setPrev(prv);
		carrNode.clear();
		this.size--;
		return true;
	}

	@Override
	public int removeAll(Car obj) {
		int count = 0;
		while (this.contains(obj)) {
			remove(this.firstIndex(obj));
			count++;
		}
		return count;
	}

	@Override
	public Car first() {
		return this.header.getNext().getElement();
	}

	@Override
	public Car last() {
		return this.header.getPrev().getElement();
	}

	@Override
	public Car get(int index) {
		if (index < 0 || index >= this.size)
			throw new IndexOutOfBoundsException();

		int count = 0;
		DNode<Car> c = header.getNext();

		while (count != index) {
			c = c.getNext();
			count++;
		}

		return c.getElement();
	}

	@Override
	public void clear() {
		while (this.size != 0)
			remove(0);
	}

	@Override
	public boolean contains(Car e) {
		return this.firstIndex(e) >= 0;
	}

	@Override
	public boolean isEmpty() {
		return this.size == 0;
	}

	@Override
	public int firstIndex(Car e) {
		// car on the list
		int index = 0;
		Iterator<Car> itr = this.iterator();
		Car nCar = itr.next();

		while (itr.hasNext() && nCar != e) {
			nCar = itr.next();
			index++;
		}
		if (nCar.equals(e))
			return index;
		else // haven't found the car
			return -1;
	}

	@Override
	public int lastIndex(Car e) {
		// if the car is not in the list
		if (!this.contains(e))
			return -1;

		// is on the list
		int count = 0, lIndex = 0;
		Iterator<Car> itr = this.iterator();
		Car nCar = itr.next();

		while (itr.hasNext()) {
			if (nCar.equals(e))
				lIndex = count;
			nCar = itr.next();
			count++;
		}
		return lIndex;
	}

}